package com.example.ebayspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EbaySpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(EbaySpringApplication.class, args);
	}



}
