package com.example.ebayspring;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @GetMapping
    public List<ProductController> getAllProducts() {
        // Логіка для отримання всіх продуктів
        List<ProductController> products = productService.getAllProducts();
        return products;
    }

    @PostMapping
    public ResponseEntity<ProductController> createProduct(@RequestBody ProductController product) {
        // Логіка для створення нового продукту
        ProductController createdProduct = productService.createProduct(product);
        return ResponseEntity.ok(createdProduct);
    }

}
